import React, { FC } from "react";
import {
	BarsOutlined,
	CalendarOutlined,
	FileAddOutlined,
	FilePptOutlined,
	FileTextOutlined,
	HomeOutlined,
	LineChartOutlined,
	ReadOutlined,
	TagsOutlined
} from "@ant-design/icons";

export const currentMenuList = [
	{ key: "/statistics/index", icon: <LineChartOutlined />, children: undefined, label: "数据统计" },
	{ key: "/config/index", icon: <CalendarOutlined />, children: undefined, label: "运营配置" },
	{ key: "/category/index", icon: <BarsOutlined />, children: undefined, label: "分类管理" },
	{ key: "/tag/index", icon: <TagsOutlined />, children: undefined, label: "标签管理" },
	{ key: "/article/index", icon: <FileTextOutlined />, children: undefined, label: "文章管理" },
	{
		key: "/column",
		icon: <ReadOutlined />,
		children: [
			{ key: "/column/setting/index", icon: <FilePptOutlined />, children: undefined, label: "专栏配置" },
			{ key: "/column/article/index", icon: <FileAddOutlined />, children: undefined, label: "教程配置" }
		],
		label: "教程管理"
	}
];
